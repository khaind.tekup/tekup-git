
import 'package:flutter/material.dart';

class AccountPage extends StatelessWidget {
  const AccountPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        Text('commit from main - rebase example'),
        Text('b1'),
        Text('b2'),
        Text('c1'),
        Text('c2')
      ],
    );
  }
}
