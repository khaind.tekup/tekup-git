
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        Text('commit from main - rebase example'),
        Text('b1'),
        Text('b2'),
        Text('c2'),
        Text('a1'),
        Text('a2')
      ],
    );
  }
}
